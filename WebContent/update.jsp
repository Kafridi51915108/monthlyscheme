<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update</title>
<style type="text/css">
body {background-color : #aaf2bd;
	
}
</style>
</head>
<body>
	<h1>Admin Update Page</h1>
	Hi,
	<%=(String) session.getAttribute("uid")%><hr />
	<form action="updateCont" method="post">
		<table>
			<tr>
				<td> ID:</td>
				<td><input type="text" name="eid" readonly="readonly"
					value="${emp.getId()}" /></td>
			</tr>
			<tr>
				<td>Name :</td>
				<td><input type="text" name="ename"
					value="${emp.getName()}"></td>
			</tr>
			<tr>
				<td>Mobile :</td>
				<td><input type="text" name="emobile"
					value="${emp.getMobile()}"></td>
			</tr>
			<tr>
				<td>Salary :</td>
				<td><input type="text" name="esalary"
					value="${emp.getSalary()}"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Update" /></td>
			</tr>
		</table>


	</form>
</body>
</html>