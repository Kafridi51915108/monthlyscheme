<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin</title>
<style type="text/css">
body{
background-color:#aaf2bd;
}
</style>
</head>
<body>
	<h1>Admin Home Page</h1>
	<p align="right">
		Hi,
		<%=(String) session.getAttribute("uid")%>
		<a href="admincrud">AdminCRUD</a> <a
			href="signout">Logout</a>
	</p>
	<hr />

	<table border="5">
		<tr>
			<th colspan="6">Employee Details</th>
		</tr>
		<tr>
			<th>id</th>
			<th>name</th>
			<th>mobile</th>
			<th>salary</th>
			
		</tr>
		<c:forEach items="${emp}" var="e">
			<tr style="text-align: center;">
				<td>${e.getId()}</td>
				<td>${e.getName()}</td>
				<td>${e.getMobile()}</td>
				<td>${e.getSalary()}</td>
			</tr>

		</c:forEach>
	</table>
</body>
</html>