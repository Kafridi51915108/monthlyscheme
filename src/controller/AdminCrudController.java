package controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AdminDao;
import dao.AdminImpl;

import model.empdetails;

@WebServlet({ "/admincrud", "/home", "/delete", "/insert", "/signout",
    "/update" })
public class AdminCrudController extends HttpServlet {
  private static final long serialVersionUID = 1L;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String url = request.getServletPath();
    AdminDao dao = new AdminImpl();
    empdetails emp = new  empdetails();

    if (url.equals("/admincrud")) {

      List< empdetails> list = dao.viewAll();

      request.setAttribute("emp", list);
      request.getRequestDispatcher("admincrud.jsp").forward(request, response);
    } else if (url.equals("/home")) {
      List< empdetails> list = dao.viewAll();
      request.setAttribute("emp", list);
      request.getRequestDispatcher("admin.jsp").forward(request, response);
    } else if (url.equals("/delete")) {
      int eid = Integer.parseInt(request.getParameter("eid"));
      emp.setId(eid);
      dao.delete(emp);;
      
      
      
      PrintWriter out = response.getWriter();
      response.setContentType("text/html");
      out.print(eid + " deleted successfully<br/>");
      List<empdetails> list = dao.viewAll();
      request.setAttribute("emp", list);
      request.getRequestDispatcher("admincrud.jsp").include(request, response);
      // request.getRequestDispatcher("admincrud").include(request, response);
    } else if (url.equals("/insert")) {
      request.getRequestDispatcher("insert.jsp").forward(request, response);
    } else if (url.equals("/signout")) {
      HttpSession session = request.getSession(false);
      String uid = (String) session.getAttribute("uid");
      System.out.println(uid + " logged out @ " + new Date());
      session.invalidate();
      request.setAttribute("uid", uid + "  Logged out successfully!!!");
      request.getRequestDispatcher("logout.jsp").forward(request, response);
    } else if (url.equals("/update")) {
    	Integer id = Integer.parseInt(request.getParameter("eid"));
		String name = request.getParameter("ename");
		long mobile = Long.parseLong(request.getParameter("emobile"));
		Integer salary = Integer.parseInt(request.getParameter("esalary"));
      empdetails emp1 = new  empdetails(id, name, mobile, salary);
      request.setAttribute("emp", emp1);
      request.getRequestDispatcher("update.jsp").forward(request, response);
    }

  }

}
