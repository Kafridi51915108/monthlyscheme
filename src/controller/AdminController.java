package controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AdminDao;
import dao.AdminImpl;

import model.Admin;
import model.empdetails;

@WebServlet(name = "AdminController", urlPatterns = { "/login", "/add", "/updateCont" })
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		AdminDao dao = new AdminImpl();

		if (url.equals("/login")) {
			String uid = request.getParameter("uid");
			String pass = request.getParameter("password");
			Admin admin = new Admin(uid, pass);
			HttpSession session = request.getSession(true);
			session.setAttribute("uid", uid);
			System.out.println(uid + "Logged in @" + new Date(session.getCreationTime()));

			int result = dao.adminAuthentication(admin);
			if (result > 0) {
				List<empdetails> list = dao.viewAll();
				request.setAttribute("emp", list);
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			} else {
				request.setAttribute("error", "Hi" + uid + ", Please check your credentials");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		} else if (url.equals("/add")) {
			Integer id = Integer.parseInt(request.getParameter("eid"));
			String name = request.getParameter("ename");
			long mobile = Long.parseLong(request.getParameter("emobile"));
			Integer salary = Integer.parseInt(request.getParameter("esalary"));

			empdetails empl = new empdetails(id, name, mobile, salary);
			int result = dao.add(empl);
			if (result > 0) {
				request.setAttribute("msg", id + "inserted succesfully");
			} else {
				request.setAttribute("msg", id + "duplicate entry");
			}
			List<empdetails> list = dao.viewAll();
			request.setAttribute("emp", list);
			request.getRequestDispatcher("admincrud.jsp").forward(request, response);
		} else if (url.equals("/updateCont")) {
			Integer id = Integer.parseInt(request.getParameter("eid"));
			String name = request.getParameter("ename");
			long mobile = Long.parseLong(request.getParameter("emobile"));
			Integer salary = Integer.parseInt(request.getParameter("esalary"));

			empdetails empl = new empdetails(id, name, mobile, salary);

			dao.update(empl);
			List<empdetails> list = dao.viewAll();
			request.setAttribute("emp", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);

		}
	}

}
