package dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;

import model.empdetails;
import util.DbUtil;
import util.Query;


public class AdminImpl implements AdminDao {
  int result;
  PreparedStatement pst;
  ResultSet rs;
 
  @Override
  public int adminAuthentication(Admin admin) {
	  result = 0;
		try {
			pst = DbUtil.getcon().prepareStatement(Query.adminLogin);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;

			}
		} catch (ClassNotFoundException | SQLException e) {

		}
		return result;
  }

  @Override
  public List<empdetails> viewAll() {
    List<empdetails> list = new ArrayList<empdetails>();
    try {
      pst = DbUtil.getcon().prepareStatement(Query.viewall);
      rs = pst.executeQuery();
      while (rs.next()) {
    	  empdetails employ = new empdetails(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getInt(4));
			list.add(employ);
        
      }
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception occurs in view All ");
    }
    return list;
  }

  @Override
  public int add(empdetails info) {
   try {
    pst = DbUtil.getcon().prepareStatement(Query.add);
    pst.setInt(1, info.getId());
    pst.setString(2, info.getName());
    pst.setLong(3, info.getMobile());
    pst.setInt(4,info.getSalary());
    result = pst.executeUpdate();
  
  } catch (ClassNotFoundException | SQLException e) {
   
  }
   
    return result;
  }



  @Override
  public void delete(empdetails info) {
    try {
      pst = DbUtil.getcon().prepareStatement(Query.delete);
      pst.setInt(1, info.getId());
      pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      
    }
  }

  @Override
  public void update(empdetails edit) {
    try {
      pst = DbUtil.getcon().prepareStatement(Query.update);
      pst.setString(1, edit.getName());
      pst.setLong(2, edit.getMobile());
      pst.setInt(3,edit.getSalary());
      pst.setInt(4,edit.getId());
      pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      
    }
    
  }

}
