package dao;



import java.util.List;

import model.Admin;

import model.empdetails;



public interface AdminDao {
  public int adminAuthentication(Admin admin);

  public List<empdetails> viewAll();

  public int add(empdetails info);

  public void update(empdetails edit);

  public void delete(empdetails info);




}
