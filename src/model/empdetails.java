package model;

public class empdetails {

	private Integer id;
	private String name;
	private long mobile;
	private Integer salary;
	
public empdetails() {
	// TODO Auto-generated constructor stub
}

public empdetails(int id, String name, long mobile, int salary) {
	super();
	this.id = id;
	this.name = name;
	this.mobile = mobile;
	this.salary = salary;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public long getMobile() {
	return mobile;
}

public void setMobile(long mobile) {
	this.mobile = mobile;
}

public int getSalary() {
	return salary;
}

public void setSalary(int salary) {
	this.salary = salary;
}

	
	
	
}
